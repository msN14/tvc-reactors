# TVC Reactors

Cantera based Trapped Vortex Combustor (TVC) reactors. TVC has two inlets, cavity and main flow inlets and two regions of reaction, cavity and main duct.

## reactor-zero

1. TVC is approximated as a single perfectly stirred reactor (PSR)
2. This is inaccurate because of the TVC is a partically premixed combustor where cavity equivalence ratio and main flow equivalence ratio distributions are different.

## reactor-one

1. In this reactor, TVC is approximated as two PSR reactors to simulate combustion in cavity and main flow.
2. The reactor is used to study the effect of inlet equivalence ratio and mass flow rate rations of the cavity and main flow.
3. A mixer without reaction is used to connect the cavity flow and main flow. After the sudden mix, the combustion is approximated a PSR in a single reactor.

## reactor-two

1. Include the entrainment of main flow into the cavity and reacting in the cavity reaction zone

