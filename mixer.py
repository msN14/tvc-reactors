


import cantera as ct

# Use air for stream a.
gas_a = ct.Solution('air.yaml')
gas_a.TPX = 300.0, ct.one_atm, 'O2:0.21, N2:0.78, AR:0.01'
rho_a = gas_a.density


# Use GRI-Mech 3.0 for stream b (methane) and for the mixer. If it is desired
# to have a pure mixer, with no chemistry, use instead a reaction mechanism
# for gas_b that has no reactions.
gas_b = ct.Solution('gri30.yaml')
gas_b.TPX = 300.0, ct.one_atm, 'CH4:0.011339223, O2:0.065341497, N2:0.71497947,\
NO:7.463364e-5, NO2: 1.4396468e-5, CO:0.016904565, CO2:0.051212733'

rho_b = gas_b.density

# Create reservoirs for the two inlet streams and for the outlet stream.  The
# upsteam reservoirs could be replaced by reactors, which might themselves be
# connected to reactors further upstream. The outlet reservoir could be
# replaced with a reactor with no outlet, if it is desired to integrate the
# composition leaving the mixer in time, or by an arbitrary network of
# downstream reactors.
res_a = ct.Reservoir(gas_a) # air
res_b = ct.Reservoir(gas_b) # exit

downstream = ct.Reservoir(gas_b)

# Create a reactor for the mixer. A reactor is required instead of a
# reservoir, since the state will change with time if the inlet mass flow
# rates change or if there is chemistry occurring.
# gas_a.TPX = 300.0, ct.one_atm, 'O2:0.21, N2:0.78, AR:0.01'
mixer = ct.IdealGasReactor(gas_b)

# My code
mixer.chemistry_enabled = False #Flase means no reaction in Mixer
mixer.energy_enabled = True #Only energy equation is solved

# create two mass flow controllers connecting the upstream reservoirs to the
# mixer, and set their mass flow rates to values corresponding to
# stoichiometric combustion.

mdot_tvc = (11.025 + 0.386 + 3.259 + 0.286)*1e-3 # [kg/s]
mdot_dilution = 11.025*1e-3 # [kg/s]

mfc1 = ct.MassFlowController(res_a, mixer, mdot=mdot_dilution)
mfc2 = ct.MassFlowController(res_b, mixer, mdot=mdot_tvc)

# connect the mixer to the downstream reservoir with a valve.
outlet = ct.Valve(mixer, downstream, K=10.0)

sim = ct.ReactorNet([mixer])

# Since the mixer is a reactor, we need to integrate in time to reach steady
# state
sim.advance_to_steady_state()

# view the state of the gas in the mixer
print(mixer.thermo.report())