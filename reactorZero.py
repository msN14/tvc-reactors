"""
*******************************************************************************
                            TVC REACTOR ONE (PSR)
...............................................................................
DESCRIPTION OF THE FUNCTION:
    1. Reactor to estimate optimum working conditions
    2. Reactor network is formulated using PSR concept along with fluid
       mixers
...............................................................................
REFERENCES:
    1. Assessment of rich-burn, quick-mix, lean-burn trapped vortex combustor
       for stationary gas turbines, Douglas L. Straub
...............................................................................
DEVELOPER INFORMATION: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
#---------------------------------------------[Imports]------------------------
import cantera as ct
import numpy as np
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# pressure (P)                                  [Pa]            
# temperature (T)                               [K]
# equivalence ratio of cavity inlet (ERC)       [-]
# equivalence ratio of main inlet (ERO)         [-]
# overall equivalence ratio (ERO)               [-]
# mass flow rate of main flow (mdot_main)       [kg/s]
# mass flow rate of cavity flow (mdot_cavity)   [kg/s]
#------------------------------------------------------------------------------

#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<[Reactor State Conditions]<<<<<<<

def wsr(pressure, temp, ERO):
    
    """
    Reactor :: PSR:: Calculations
    > Mass flow rate is adjusted for keeping required residence time
    
    Parameters ::
    > ERO = Equivalence ratio
    > Temperature = Mass flow rate of cavity flow
    > Pressure = Mass flow rate of main flow
    
    Return ::
    > Reactor Temperature (Treactor) [K]
    > Cavity XNOX, XCO, CH4 * 1e6 for [ppm]
    
    """
    
    gas = ct.Solution('gri30.xml')
    
    equiv_ratio = ERO
    gas.TP = temp, pressure
    gas.set_equivalence_ratio(equiv_ratio, 'CH4:1.0', 'O2:1.0, N2:3.76')
    inlet = ct.Reservoir(gas)
    
    gas.equilibrate('HP')
    combustor = ct.IdealGasReactor(gas)
    combustor.volume = 50*20*240*1e-9
    
    exhaust = ct.Reservoir(gas)
    
    def mdot(t):
        return combustor.mass / residence_time
    
    inlet_mfc = ct.MassFlowController(inlet, combustor, mdot=mdot)
    outlet_mfc = ct.PressureController(combustor, exhaust, master=inlet_mfc, K=0.01)
    
    sim = ct.ReactorNet([combustor])
    
    states = ct.SolutionArray(gas, extra=['tres'])
    
    residence_time = 0.01  # starting residence time
    
    while combustor.T > 300:
        sim.set_initial_time(0.0)  # reset the integrator
        sim.advance_to_steady_state()
        states.append(combustor.thermo.state, tres=residence_time)
        print('tres = {:.2e}; T = {:.1f}; NO = {:.10e}; CO = {:.10e}'
              .format(residence_time, combustor.T, 
              states.X[-1,gas.species_index('NO')], states.X[-1,gas.species_index('CO')]))
        residence_time *= 0.99  # decrease the residence time for the next iteration
    
    # Heat release rate [W/m^3]
    # Q = - np.sum(states.net_production_rates * states.partial_molar_enthalpies, axis=1)
    
    #-----------------------------------------[Dict Result Outcome]------------
    resultDict = {'T Reactor': np.round(states.T,0),
    'NO ppm': np.round(states.X[:-1,gas.species_index('NO')]*1e6,2),
    'CO ppm': np.round(states.X[:-1,gas.species_index('CO')]*1e6,2),
    'CH4 ppm': np.round(states.X[:-1,gas.species_index('CH4')]*1e6,2),
    'tres [ms]' : np.round(states.tres[:-1]*1e3,3)
    }
    
    return resultDict

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>[Reactor State Conditions]>>>>>>>



