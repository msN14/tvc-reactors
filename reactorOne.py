"""
*******************************************************************************
                            TVC REACTOR ONE (PSR-MIX-PSR)
...............................................................................
DESCRIPTION OF THE FUNCTION:
    1. Reactor to estimate optimum working conditions
    2. Reactor network is formulated using PSR concept along with fluid
       mixers
...............................................................................
REFERENCES:
    1. Assessment of rich-burn, quick-mix, lean-burn trapped vortex combustor
       for stationary gas turbines, Douglas L. Straub
...............................................................................
DEVELOPER INFORMATION: NISANTH M. S, nisanthm@iisc.ac.in
*******************************************************************************
"""
#---------------------------------------------[Imports]------------------------
import cantera as ct
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# pressure (P)                                  [Pa]            
# temperature (T)                               [K]
# equivalence ratio of cavity inlet (ERC)       [-]
# equivalence ratio of main inlet (ERO)         [-]
# overall equivalence ratio (ERO)               [-]
# mass flow rate of main flow (mdot_main)       [kg/s]
# mass flow rate of cavity flow (mdot_cavity)   [kg/s]
#------------------------------------------------------------------------------

#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<[Reactor State Conditions]<<<<<<<

def Reactor(mdot_cavity, mdot_main, cavity_tres, main_tres, ERC, ERM, 
            pressure=101325.0, temperature=300.0):
    
    """
    Reactor :: PSR -> MIX -> PSR :: Calculations
    > Volume is adjusted for required mass flow rate and residence time 
      (iteration)
    
    Parameters ::
    > ERC = Equivalence ratio of cavity inlet
    > ERM = Equivalence ratio of main inlet
    > mdot_cavity = Mass flow rate of cavity flow
    > mdot_main = Mass flow rate of main flow
    > cavity_tres = residence time in cavity reactor
    > main_tres = residence time in main reactor
    
    Return ::
    > Cavity Reactor Temperature (Tcavity) [K]
    > Mixer outlet temperature (Tmixer) [K]
    > Main Reactor Temperature (Tmain) [K]
    > Cavity XNOX, XCO, CH4 * 1e6 for [ppm]
    > Main XNOx, XCO, CH4 * 1e6 for [ppm]
    
    """
    
    #-----------------------------------------[Supporting Functions]-----------
    def Volume(mass, Temp, MW, Pres):
        return (mass*8314*Temp)/(MW* Pres) # units are tested !
    
    def tres():
        tc = (cavityReactor.mass/mdot_cavity)
        tm = (mainReactor.mass/(mdot_cavity+ mdot_main))
        return tc, tm
    #-----------------------------------------[Cavity Reactor Definitions]-----
    cavityGas = ct.Solution('gri30.xml')
    cavityGas.TP = temperature, pressure
    cavityGas.set_equivalence_ratio(ERC, 'CH4:1.0', 'O2:1.0, N2:3.76')
    cavityReservoir = ct.Reservoir(cavityGas)
    YCH4_cavity = cavityGas.Y[cavityGas.species_index('CH4')]
    # print("YCH4_cavity=",YCH4_cavity)
    cavityGas.equilibrate('HP')
    cavityReactor = ct.IdealGasReactor(cavityGas)
    #-----------------------------------------[Main Reactor Definitions]-------
    mainGas = ct.Solution('gri30.xml')
    mainGas.TP = temperature, pressure
    mainGas.set_equivalence_ratio(ERM, 'CH4:1.0', 'O2:1.0, N2:3.76')
    mainReservoir = ct.Reservoir(mainGas)
    YCH4_main = mainGas.Y[mainGas.species_index('CH4')]
    # print("YCH4_main=",YCH4_main)
    mainGas.equilibrate('HP')
    mainReactor = ct.IdealGasReactor(mainGas)
    #-----------------------------------------[Mixer for mixing]---------------
    mixer = ct.IdealGasReactor(mainGas)
    mixer.chemistry_enabled = False #Flase means no reaction in Mixer
    mixer.energy_enabled = True #Only energy equation is solved
    #-----------------------------------------[Exhaust Definition]-------------
    exhaust = ct.Reservoir(mainGas)
    #-----------------------------------------[Reactor Connections]------------
    #Mass flow controllers
    cavityFlow_MFC = ct.MassFlowController(cavityReservoir,
                                           cavityReactor, mdot=mdot_cavity)
    mainFlow_MFC = ct.MassFlowController(mainReservoir, mixer, mdot=mdot_main)
    
    #Connection b/w reactors 
    c1 = ct.PressureController(cavityReactor, mixer, 
                               master=cavityFlow_MFC, K=1e-5)
    pressureValveCoefficient = 0.01
    v1 = ct.Valve(upstream=mixer, downstream=mainReactor,
                  K=pressureValveCoefficient)
    
    #Exhaust of the final reactor
    pressureValveCoefficient = 0.01
    e1 = ct.Valve(upstream=mainReactor, downstream=exhaust,
                  K=pressureValveCoefficient)
    
    #Defining the reactor
    tvc = ct.ReactorNet([cavityReactor, mixer, mainReactor])
    #-----------------------------------------[Solution Save Settings]---------
    cavityState = ct.SolutionArray(cavityGas, extra=['tres'])
    mixerState = ct.SolutionArray(mainGas, extra=['tres'])
    mainState = ct.SolutionArray(mainGas, extra=['tres'])
    #--------------------------------------------------------------------------
    
    #=========================================[SOLVING]========================
    
    # if cavityReactor.T > 500:
        
    for i in range(10):
        tvc.set_initial_time(0.0)  # reset the integrator
        tvc.advance_to_steady_state()
        
        cavityState.append(cavityReactor.thermo.state, tres=cavity_tres)
        mixerState.append(mixer.thermo.state, tres=cavity_tres)
        mainState.append(mainReactor.thermo.state, tres=main_tres)
        
        # Redefining combustor volumes (cavity and main) based on the new
        # states with fixed mass flow rate and residence time and variable
        # combustor states
        cavityReactor.volume = Volume(mdot_cavity*cavity_tres, cavityGas.T,
                                    cavityGas.mean_molecular_weight, 
                                    cavityGas.P )
        mainReactor.volume = Volume((mdot_cavity + mdot_main)*main_tres,
                                   mainGas.T, mainGas.mean_molecular_weight, 
                                   mainGas.P )
        # print("combustor temperature = ", cavityReactor.T, mixer.T, mainReactor.T )
    # print(cavityState.X[-1, mainGas.species_index('NO')])
    
    #-----------------------------------------[Emission Index and ERO]---------
    mdot_cavity_fuel = mdot_cavity * YCH4_cavity
    mdot_cavity_air = mdot_cavity - mdot_cavity_fuel
    mdot_main_fuel = mdot_main * YCH4_main
    mdot_main_air = mdot_main - mdot_main_fuel
    mdot_fuel = mdot_cavity_fuel + mdot_main_fuel
    mdot_air = mdot_cavity_air + mdot_main_air
    mdot_tvc = mdot_fuel + mdot_air
    
    ERO = 17.11 * (mdot_fuel/mdot_air)
    mdot_UHC_exhaust = mdot_tvc * mainState.Y[-1,mainGas.species_index('CH4')]
    mdot_CO_exhaust = mdot_tvc * mainState.Y[-1,mainGas.species_index('CO')]
    mdot_NO_exhaust = mdot_tvc * mainState.Y[-1,mainGas.species_index('NO')]
    mdot_NO2_exhaust = mdot_tvc * mainState.Y[-1,mainGas.species_index('NO2')]
    
    mdot_NOx_exhaust = mdot_NO_exhaust + mdot_NO2_exhaust #NOx MFR
    
    EIUHC = (mdot_UHC_exhaust/mdot_fuel)*1000
    EICO = (mdot_CO_exhaust/mdot_fuel)*1000
    EINO = (mdot_NO_exhaust/mdot_fuel)*1000
    EINO2 = (mdot_NO2_exhaust/mdot_fuel)*1000
    EINOx = (mdot_NOx_exhaust/mdot_fuel)*1000
    
    Hc = 50e6
    # equation for combustion efficiency calculation
    eff=(1-(10109*(EICO/Hc))-(EIUHC/1000))*100
    
    print("Cavity Temp = ", round(cavityReactor.T,2))
    #-----------------------------------------[Dict Result Outcome]------------
    resultDict = {'T Cavity': round(cavityReactor.T,2),
                  'T Mixer': round(mixer.T,2),
                  'T Main' : round(mainReactor.T,2),
    'NO Cavity ppm':round(cavityState.X[-1,cavityGas.species_index('NO')]*1e6,2),
    'CO Cavity ppm':round(cavityState.X[-1,cavityGas.species_index('CO')]*1e6,2),
    'CH4 Cavity ppm':round(cavityState.X[-1,cavityGas.species_index('CH4')]*1e6,2),
    
    'NO Main ppm':round(mainState.X[-1,mainGas.species_index('NO')]*1e6,2),
    'NO2 Main ppm':round(mainState.X[-1,mainGas.species_index('NO2')]*1e6,2),
    'CO Main ppm':round(mainState.X[-1,mainGas.species_index('CO')]*1e6,2),
    'CH4 Main ppm':round(mainState.X[-1,mainGas.species_index('CH4')]*1e6,2),
    
    'EINO' : round(EINO,2),
    'EINO2' : round(EINO2,2),
    'EINOx' : round(EINOx,2),
    'EICO' : round(EICO,2),
    'EIUHC': round(EIUHC,2),
    'Eff' : round(eff,2),
    'ERO': round(ERO,2),
    'ERC': round(ERC,2),
    'ERM': round(ERM,2),
    'MR' : round((mdot_main/mdot_cavity),2),
    'Cavity tres [ms]' : round(cavity_tres*1e3,3), # in milli sec [ms]
    'Main tres [ms]' : round(main_tres*1e3,3), # in milli sec [ms]
    'Pressure' : round(pressure,2),
    'Temperature' : round(temperature,2)
    }
    
    return resultDict

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>[Reactor State Conditions]>>>>>>>
